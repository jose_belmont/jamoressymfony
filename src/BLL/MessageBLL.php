<?php


namespace App\BLL;

use App\Entity\Mensajes;
use App\Entity\Tickets;
use App\Entity\Users;

class MessageBLL extends BaseBLL{

    public function toArray($message) {
        if ( is_null ($message))
            return null;

        if (!($message instanceof Mensajes))
            throw new \Exception("La entidad no es un ticket");

        return [
            'id' => $message->getId(),
            'destinatario' => $message->getDestinatario()->getNombre(),
            'mensaje' => $message->getMensaje()
        ];
    }

    public function getAll() {
        $tickets = $this->em->getRepository(Tickets::class)->findBy(['usuario' => $this->getUser()]);
        return $this->entitiesToArray($tickets);
    }

    public function nuevo($data)
    {
        $message = new Mensajes();
        $message->setRemitente($this->getUser());
        $message->setDestinatario($this->em->getRepository(Users::class)->find($data['destinatario']));
        $message->setMensaje($data['mensaje']);

        return $this->guardaValidando($message);
    }
}