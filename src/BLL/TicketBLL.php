<?php


namespace App\BLL;

use App\Entity\Events;
use App\Entity\Tickets;

class TicketBLL extends BaseBLL{

    public function toArray($ticket) {
        if ( is_null ($ticket))
            return null;

        if (!($ticket instanceof Tickets))
            throw new \Exception("La entidad no es un ticket");

        return [
            'id' => $ticket->getId(),
            'evento' => $ticket->getEvento()->getTitulo(),
            'cantidad' => $ticket->getCantidad()
        ];
    }

    public function getAll() {
        $tickets = $this->em->getRepository(Tickets::class)->findBy(['usuario' => $this->getUser()]);
        return $this->entitiesToArray($tickets);
    }

    public function nuevo($data)
    {
        $ticket = new Tickets();
        $ticket->setComprador($this->getUser());
        $ticket->setEvento($this->em->getRepository(Events::class)->find($data['evento']));
        $ticket->setCantidad($data['cantidad']);

        return $this->guardaValidando($ticket);
    }
}