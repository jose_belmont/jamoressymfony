<?php

namespace App\BLL;


use App\Entity\Users;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class UserBLL extends BaseBLL
{
    /**
     * @var UserPasswordEncoderInterface $encoder
     */
    private $encoder;

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    public function setJWTManager(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    public function setEncoder(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function nuevo($data)
    {
        $user = new Users();

        $user->setNombre($data['nombre']);
        $user->setApellidos($data ['apellidos']);
        $user->setPass($this->encoder->encodePassword($user, $data ['password']));
        $user->setEmail($data ['email']);
        $user->setFechaNacimiento(new \DateTime($data ['fechaNacimiento']));
        $user->setUser($data['user']);
        $user->setRole('ROLE_USER');
        $user->setAvatar('default.png');

        return $this->guardaValidando($user);
    }

    public function getAll()
    {
        $users = $this->em->getRepository(Users::class)->findAll();

        return $this->entitiesToArray($users);
    }

    public function toArray($user)
    {
        if ( is_null ($user))
            return null;

        if (!($user instanceof Users))
            throw new \Exception("La entidad no es un User");

        return [
            'id' => $user->getId(),
            'nombre' => $user->getNombre(),
            'email' => $user->getEmail(),
            'role' => $user->getRole()
        ];
    }

    public function profile()
    {
        $user = $this->getUser();

        return $this->toArray($user);
    }

    public function cambiaPassword($nuevoPassword)
    {
        $user = $this->getUser();

        $user->setPassword($this->encoder->encodePassword($user, $nuevoPassword));

        return $this->guardaValidando($user);
    }

    public function getTokenByEmail($email)
    {
        $user = $this->em->getRepository(Users:: class )
            ->findOneBy(array('email'=>$email));

        if ( is_null ($user))
            throw new AccessDeniedHttpException('Usuario no autorizado');

        return $this->jwtManager->create($user);
    }
}