<?php


namespace App\BLL;


use App\Entity\Categorias;
use App\Entity\Events;
use App\Entity\Provincias;
use App\Service\FileUploader;
use \Datetime;

class EventBLL extends BaseBLL
{
    /**
     * @var FileUploader $uploader
     */
    private $uploader;

    public function setUploader(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function eliminaEvento($id)
    {
        $evento = $this->em->getRepository(Events::class)->find($id);

        $this->em->remove($evento);
        $this->em->flush();
    }

    public function nuevo($data)
    {
        $event = new Events();
        $event->setTitulo($data['titulo']);
        $event->setPrecioEntrada($data['precio']);
        $event->setImagen('default.png');
        $event->setFecha(DateTime::createFromFormat('j-m-Y', $data['fechaHoraEvento']));
        $event->setPrecioEntrada($data['numeroEntradas']);
        $event->setDescripcion($data['descripcion']);
        $event->setDireccion($data['direccion']);
        $event->setEntradasDisponibles($data['entradasDisponibles']);
        $event->setEntradasMaximas($data['entradasMaximas']);

        $categoria = $this->em->getRepository(Categorias::class)->find($data['categoria']);
        $event->setCategoria($categoria);

        $provincia = $this->em->getRepository(Provincias::class)->find($data['provincia']);
        $event->setProvincia($provincia);

        $event->setCreador($this->getUser());

        return $this->guardaValidando($event);
    }

    public function guardaEvento(Events $event)
    {
        $this->em->persist($event);
        $this->em->flush();
    }

    public function getEventosFiltrados($titulo, $ciudad, $categoria, $order)
    {
        $stm = $this->em->getRepository(Events::class)->createQueryBuilder('a');
        if (isset($titulo)) {
            $stm->where($stm->expr()->eq('a.titulo', ':name'))
                ->setParameter('name', $titulo);
        }
        /*if (isset($ciudad)) {
            $stm->andWhere($stm->expr()->gte('a.ciudad', ':ciudad'))
                ->setParameter('ciudad', $ciudad);
        }*/
        if (isset($categoria)) {
            $stm->andWhere($stm->expr()->gte('a.categoria', ':categoria'))
                ->setParameter('categoria', $categoria);
        }

        /*if (isset($order)) {
            $stm->andWhere($stm->expr()->like('a.order', ':order'))
                ->setParameter('order', "%" . $order . "%");
        }*/

        return $stm->getQuery()->getResult();
        /*$eventos = $this->em->getRepository(Events:: class )->getEventosFiltrados($titulo, $ciudad, $categoria, $order);

        return $this->entitiesToArray($eventos);*/
    }

    public function update(Events $event, array $data)//se usa
    {
        $event->setTitulo($data['titulo']);
        $event->setPrecioEntrada($data['precio']);
        $event->setImagen('default.png');
        $event->setFecha(DateTime::createFromFormat('j-m-Y', $data['fechaHoraEvento']));
        $event->setPrecioEntrada($data['numeroEntradas']);
        $event->setDescripcion($data['descripcion']);
        $event->setDireccion($data['direccion']);
        $event->setEntradasDisponibles($data['entradasDisponibles']);
        $event->setEntradasMaximas($data['entradasMaximas']);

        $categoria = $this->em->getRepository(Categorias::class)->find($data['categoria']);
        $event->setCategoria($categoria);

        $provincia = $this->em->getRepository(Provincias::class)->find($data['provincia']);
        $event->setProvincia($provincia);

        if ($event->getCreador() == $this->getUser())
            return $this->guardaValidando($event);
        throw new \Exception("No tienes permisos para modificar este evento");
    }

    public function toArray($evento)
    {
        if ( is_null ($evento))
            return null;

        if (!($evento instanceof Events))
            throw new \Exception("La entidad no es un Producto");

        return [
            'id' => $evento->getId(),
            'fechaHoraEvento' => $evento->getFecha()->format("d-m-Y H:i:s"),
            'titulo' => $evento->getTitulo(),
            'precioEntrada' => $evento->getPrecioEntrada(),
            'imagen' => $evento->getImagen(),
            'categoria' => $evento->getCategoria()->getNombre()
        ];
    }

    public function cambiaImagen(Events $event, $imagenB64)
    {
        $fileName = $this->uploader->saveB64Imagen($imagenB64);

        $event->setImagen($fileName);

        return $this->guardaValidando($event);
    }
}