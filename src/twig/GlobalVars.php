<?php
/**
 * Created by PhpStorm.
 * User: belmont
 * Date: 19/2/18
 * Time: 11:11
 */

namespace App\twig;


use App\Entity\Categorias;
use App\Entity\Provincias;
use Doctrine\Common\Persistence\ObjectManager;

class GlobalVars
{
    private $em;
    private $idioma;


    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function getCategorias()
    {
        $categorias = $this->em->getRepository(Categorias::class)->findAll();
        return $categorias;
    }


    public function getProvincias()
    {
        $provincias = $this->em->getRepository(Provincias::class)->findAll();
        return $provincias;
    }


    public function getIdioma()
    {
        return $this->idioma;
    }
}