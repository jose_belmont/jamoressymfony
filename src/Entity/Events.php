<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventsRepository")
 */
class Events
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=600, nullable=true)
     */
    private $imagen;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message = "Hace falta insertar un título.")
     * @Assert\Type("string")
     */
    private $titulo;

    /**
     * @ORM\Column(type="string", length=500)
     * @Assert\NotBlank(message = "Hace falta insertar una descripción")
     * @Assert\Type("string")
     */
    private $descripcion;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message = "Es necesaria la fecha del evento")
     * @Assert\Type("datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message = "La dirección del evento es necesaria.")
     * @Assert\Type("string")
     */
    private $direccion;

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\NotBlank(message = "Es necesario el precio de la entrada.")
     * @Assert\Type("float")
     */
    private $precio_entrada;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message = "Debe especificar la cantidad máxima de entradas")
     * @Assert\Type("integer")
     */
    private $entradas_maximas;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     */
    private $entradas_disponibles;

    /**
     * @ORM\ManyToOne(targetEntity="Categorias")
     * @ORM\JoinColumn(name="categoria", referencedColumnName="id")
     */
    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="Provincias")
     * @ORM\JoinColumn(name="provincia", referencedColumnName="id")
     */
    private $provincia;


    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="creador", referencedColumnName="id")
     */
    private $creador;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Events
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     * @return Events
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     * @return Events
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     * @return Events
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     * @return Events
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     * @return Events
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrecioEntrada()
    {
        return $this->precio_entrada;
    }

    /**
     * @param mixed $precio_entrada
     * @return Events
     */
    public function setPrecioEntrada($precio_entrada)
    {
        $this->precio_entrada = $precio_entrada;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntradasMaximas()
    {
        return $this->entradas_maximas;
    }

    /**
     * @param mixed $entradas_maximas
     * @return Events
     */
    public function setEntradasMaximas($entradas_maximas)
    {
        $this->entradas_maximas = $entradas_maximas;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntradasDisponibles()
    {
        return $this->entradas_disponibles;
    }

    /**
     * @param mixed $entradas_disponibles
     * @return Events
     */
    public function setEntradasDisponibles($entradas_disponibles)
    {
        $this->entradas_disponibles = $entradas_disponibles;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     * @return Events
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * @param mixed $provincia
     * @return Events
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreador()
    {
        return $this->creador;
    }

    /**
     * @param mixed $creador
     * @return Events
     */
    public function setCreador($creador)
    {
        $this->creador = $creador;
        return $this;
    }
    public function getImagenString()
    {
        return '/images/events/'.$this->getImagen();
    }


}
