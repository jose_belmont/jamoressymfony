<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TicketsRepository")
 */
class Tickets
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="comprador", referencedColumnName="id")
     */
    private $comprador;

    /**
     * @ORM\ManyToOne(targetEntity="Events")
     * @ORM\JoinColumn(name="evento", referencedColumnName="id")
     */
    private $evento;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha_compra;

    /**
     * @ORM\Column(type="integer")
     */
    private $cantidad;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $precio;


    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $costeTotal;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Tickets
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getComprador()
    {
        return $this->comprador;
    }

    /**
     * @param mixed $comprador
     * @return Tickets
     */
    public function setComprador($comprador)
    {
        $this->comprador = $comprador;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEvento()
    {
        return $this->evento;
    }

    /**
     * @param mixed $evento
     * @return Tickets
     */
    public function setEvento($evento)
    {
        $this->evento = $evento;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCompra()
    {
        return $this->fecha_compra;
    }

    /**
     * @param mixed $fecha_compra
     * @return Tickets
     */
    public function setFechaCompra($fecha_compra)
    {
        $this->fecha_compra = $fecha_compra;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     * @return Tickets
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     * @return Tickets
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCosteTotal()
    {
        return $this->costeTotal;
    }

    /**
     * @param mixed $costeTotal
     * @return Tickets
     */
    public function setCosteTotal($costeTotal)
    {
        $this->costeTotal = $costeTotal;
        return $this;
    }

}
