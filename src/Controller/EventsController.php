<?php

namespace App\Controller;

use App\Entity\Categorias;
use App\Entity\Events;
use App\Entity\Provincias;
use App\Entity\Tickets;
use App\Entity\Users;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Date;


class EventsController extends Controller
{
    /**
     * @Route("/", name="events")
     */
    public function index()
    {
        $eventos = $this->getDoctrine()
            ->getRepository(Events::class)->findAll();


        return $this->render(
            'events/index-events.html.twig',
            array(
                    'titulo'=>'Index',
                    'eventos' =>$eventos
            )
        );
    }


    /**
     * @Route("/events/new", name="events_new")
     */

    //Creación de formulario de nuevo Evento
    public function formularioEvento(Request $request)
    {
        $categorias = $this->getDoctrine()->getRepository(Categorias::class)->findAll();
        $provincias = $this->getDoctrine()->getRepository(Provincias::class)->findAll();

        $categoriasSelect = array();
        foreach ($categorias as $categoria) {
            $categoriasSelect[$categoria->getNombre()] = $categoria->getId();
        }

        $provinciasSelect = array();
        foreach ($provincias as $provincia) {
            $provinciasSelect[$provincia->getNombre()] = $provincia->getId();
        }

        $nuevoEvento = new Events();
        $form = $this->createFormBuilder($nuevoEvento,
            array('attr' => array('class' => 'form-signin')))

            ->add('titulo', TextType::class ,
                array('label' => 'Titulo del Evento',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Titulo',
                        'trim'=>true)
                )
            )

            ->add('Categoria', ChoiceType::class, array(
                    'label' => 'Selector Categoria Del Evento',
                    'attr' =>
                        array('class' => 'form-control'),
                    'choices' => $categoriasSelect)
            )
            ->add('Provincia', ChoiceType::class, array(
                    'label' => 'Selector Provincia Del Evento',
                    'attr' =>
                        array('class' => 'form-control'),
                    'choices' => $provinciasSelect)
            )

            ->add('direccion', TextType::class ,
                array('label' => 'dirección Del Evento',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'direccion Del Evento',
                        'trim'=>true)
                )
            )

            ->add('fecha', DateType::class ,
                array('label' => 'Fecha del Evento', 'attr' =>
                    array('class' => 'form-control mb-3'),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',

                )
            )

            ->add('descripcion', TextareaType::class ,
                array('label' => 'Descripción Del Evento',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Descripción Del Evento',
                        'trim'=>true)
                )
            )

            ->add('precio_entrada', NumberType::class ,
                array('label' => 'Precio Entrada',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'value' => 0
                    )
                )
            )

            ->add('entradas_maximas', IntegerType::class ,
                array('label' => 'Entradas Máximas',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'value' => 0
                    )
                )
            )

            ->add('imagen', FileType::class ,
                array('label' => 'Imagen del Evento',
                    'attr' => array(
                        'class' => 'form-control mb-3',

                    )
                )
            )

            ->add('Save',SubmitType::class,
                array('label'=>'Crea o modifica el evento', 'attr' =>
                    array('class' => 'btn btn-lg btn-primary btn-block')))

            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //si es correcto, obtenemos la imagen
            $archivo = $nuevoEvento->getImagen();

            //doble número random para que no se repita el nombre de la foto
            $nombreArchivo = rand (0, 1000000). rand (0, 1000000) .'.'.$archivo->guessExtension();

            //events_directory del services.yaml
            $archivo->move($this->getParameter('events_directory'),$nombreArchivo);

            //sobreescribimos la foto
            $nuevoEvento->setImagen($nombreArchivo);

            $nuevoEvento->setEntradasDisponibles($nuevoEvento->getEntradasMaximas());

            $em = $this->getDoctrine()->getManager();

            //para poder coger el valor de la categoria en otra tabla
            $nuevoEvento->setCategoria(
                $em->getRepository(Categorias::class)->findOneBy(array('id'=>$nuevoEvento->getCategoria()))
            );

            //para poder coger el valor de la categoria en otra tabla
            $nuevoEvento->setProvincia(
                $em->getRepository(Provincias::class)->findOneBy(array('id'=>$nuevoEvento->getProvincia()))
            );

            $nuevoEvento->setCreador(
                $em->getRepository(Users::class)->find($this->getUser()->getId())
            );

            $em->persist($nuevoEvento);
            $em->flush();
            return $this->redirectToRoute('events');
        }

        return $this->render(
            'events/form-event.html.twig',
            array(
                'titulo'=>'Index',
                'form' =>$form->createView()
            )
        );

    }

    /**
 * @Route("/events/remove/{id}", name="event_delete")
 */

    //metodo para borrar
    public function borrarEvento ($id){
        $evento = $this->getDoctrine()->getRepository(Events::class)->findOneBy(array('id' => $id));
        $em = $this->getDoctrine()->getManager();
        $em->remove($evento);
        $em->flush();
        return $this->redirectToRoute('events');
    }

    /**
     * @Route("/events/edit/{id}", name="events_edit")
     */

    //Creación de formulario de edición de Evento
    public function editarEvento($id, Request $request)
    {
        $user= $this->getDoctrine()->getRepository(Users::class)->findOneBy(array('id' => $id));
        $edit= $this->getDoctrine()->getRepository(Events::class)->findOneBy(array('id' => $id));

        $edit->setCategoria('');
        $edit->setProvincia('');
        $edit->setImagen('');


        $categorias = $this->getDoctrine()->getRepository(Categorias::class)->findAll();
        $provincias = $this->getDoctrine()->getRepository(Provincias::class)->findAll();

        $categoriasSelect = array();
        foreach ($categorias as $categoria) {
            $categoriasSelect[$categoria->getNombre()] = $categoria->getId();
        }

        $provinciasSelect = array();
        foreach ($provincias as $provincia) {
            $provinciasSelect[$provincia->getNombre()] = $provincia->getId();
        }


        $form = $this->createFormBuilder($edit,
            array('attr' => array('class' => 'form-signin')))

            ->add('titulo', TextType::class ,
                array('label' => 'Titulo del Evento',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Titulo',
                        'trim'=>true)
                )
            )

            ->add('Categoria', ChoiceType::class, array(
                    'label' => 'Selector Categoria Del Evento',
                    'attr' =>
                        array('class' => 'form-control'),
                    'choices' => $categoriasSelect)
            )
            ->add('Provincia', ChoiceType::class, array(
                    'label' => 'Selector Provincia Del Evento',
                    'attr' =>
                        array('class' => 'form-control'),
                    'choices' => $provinciasSelect)
            )

            ->add('direccion', TextType::class ,
                array('label' => 'dirección Del Evento',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'direccion Del Evento',
                        'trim'=>true)
                )
            )

            ->add('fecha', DateType::class ,
                array('label' => 'Fecha del Evento', 'attr' =>
                    array('class' => 'form-control mb-3'),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',

                )
            )

            ->add('descripcion', TextareaType::class ,
                array('label' => 'Descripción Del Evento',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Descripción Del Evento',
                        'trim'=>true)
                )
            )

            ->add('precio_entrada', NumberType::class ,
                array('label' => 'Precio Entrada',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'value' => 0
                    )
                )
            )

            ->add('entradas_maximas', IntegerType::class ,
                array('label' => 'Entradas Máximas',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'value' => 0
                    )
                )
            )

            ->add('imagen', FileType::class ,
                array('label' => 'Imagen del Evento',
                    'attr' => array(
                        'class' => 'form-control mb-3',

                    )
                )
            )

            ->add('Save',SubmitType::class,
                array('label'=>'Crear Evento', 'attr' =>
                    array('class' => 'btn btn-lg btn-primary btn-block')))

            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //si es correcto, obtenemos la imagen
            $archivo = $edit->getImagen();

            //doble número random para que no se repita el nombre de la foto
            $nombreArchivo = rand (0, 1000000). rand (0, 1000000) .'.'.$archivo->guessExtension();

            //events_directory del services.yaml
            $archivo->move($this->getParameter('events_directory'),$nombreArchivo);

            //sobreescribimos la foto
            $edit->setImagen($nombreArchivo);

            $edit->setEntradasDisponibles($edit->getEntradasMaximas());

            $em = $this->getDoctrine()->getManager();

            //para poder coger el valor de la categoria en otra tabla
            $edit->setCategoria(
                $em->getRepository(Categorias::class)->findOneBy(array('id'=>$edit->getCategoria()))
            );

            //para poder coger el valor de la categoria en otra tabla
            $edit->setProvincia(
                $em->getRepository(Provincias::class)->findOneBy(array('id'=>$edit->getProvincia()))
            );

            //no puedo sacar el perfil logueado
            $edit->setCreador(
                $em->getRepository(Users::class)->find($this->getUser()->getId())
            );

            $em->persist($edit);
            $em->flush();
            return $this->redirectToRoute('events');
        }

        return $this->render(
            'events/form-event.html.twig',
            array(
                'titulo'=>'Update',
                'form' =>$form->createView()
            )
        );

    }
    /**
     * @Route("/events/my", name="myevents")
     */
    public function myEvents()
    {

        $events = $this->getDoctrine()
            ->getRepository(Events::class)->findBy(
                array(
                    'creador' => $this->getUser()->getId()
                )
            );

        return $this->render(
            'events/my.html.twig',
            array(
                'titulo'=>'Mis Eventos',
                'events' =>$events
            )
        );
    }

    /**
     * @Route("/events/{id}", name="events_id")
     */
    public function eventDetail($id,  Request $request)
    {
        $events = $this->getDoctrine()
            ->getRepository(Events::class)->findOneBy(array('id' => $id));
        $nuevaEntrada = new Tickets();
        $formEntrada = $this->createFormBuilder($nuevaEntrada,
            array('attr' => array('class' => 'form-signin')))

            ->add('cantidad', NumberType::class ,
                array('label' => 'Cantidad',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'value' => 1
                    )
                )
            )->add('precio', NumberType::class ,
                array('label' => 'Precio Entrada',
                    'attr' => array(
                        'class' => 'form-control mb-3 disabled',
                        'readonly' => 'readonly',
                        'value' => $events->getPrecioEntrada()
                    )
                )
            )

            ->add('Save',SubmitType::class,
                array('label'=>'Comprar Entradas', 'attr' =>
                    array('class' => 'btn btn-lg btn-primary btn-block')))

            ->getForm();

        $formEntrada->handleRequest($request);

        if ($formEntrada->isSubmitted() && $formEntrada->isValid()) {


            $nuevaEntrada->setFechaCompra( new \DateTime('NOW'));

            $em = $this->getDoctrine()->getManager();
            $nuevaEntrada->setComprador($this->getUser());
            $nuevaEntrada->setEvento($em->getRepository(Events::class)->find($id));

            $nuevaEntrada->setCosteTotal(($nuevaEntrada->getPrecio() * $nuevaEntrada->getCantidad()));

            $em->persist($nuevaEntrada);
            $em->flush();
            return $this->redirectToRoute('events');
        }

        return $this->render(
            'events/event-details.html.twig',
            array(
                'titulo'=>'Index',
                'events' =>$events,
                'formEntrada' =>$formEntrada->createView()
            )
        );
    }

    /**
     * @Route("/events/categoria/{id}", name="events_categoria")
     */
    //filtrar por categorias
    public function categorias($id){

        $categoria = $this->getDoctrine()
            ->getRepository(Categorias::class)->findOneBy( array ('id' => $id));
        $eventos = $this->getDoctrine()
            ->getRepository(Events::class)->findBy( array('categoria' => $categoria->getId()));


        return $this->render(
            'events/index-events.html.twig',
            array(
                'titulo'=>'Index',
                'eventos' =>$eventos
            )
        );
    }



    /**
     * @Route("/events/provincia/{id}", name="events_provincia")
     */
    //filtrar por provincias
    public function provincias($id){

        $provincia = $this->getDoctrine()
            ->getRepository(Provincias::class)->findOneBy( array ('id' => $id));
        $eventos = $this->getDoctrine()
            ->getRepository(Events::class)->findBy( array('provincia' => $provincia->getId()));


        return $this->render(
            'events/index-events.html.twig',
            array(
                'titulo'=>'Index',
                'eventos' =>$eventos
            )
        );
    }




}
