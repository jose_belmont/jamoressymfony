<?php

namespace App\Controller\REST;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AuthRestController
{
    /**
     * @Route("/auth/login")
     */
    public function getTokenAction()
    {
        return new Response('', Response:: HTTP_UNAUTHORIZED );
    }
}