<?php
/**
 * Created by PhpStorm.
 * User: belmont
 * Date: 19/2/18
 * Time: 13:04
 */

namespace App\Controller;

use App\Entity\Categorias;
use App\Entity\Events;
use App\Entity\Provincias;
use App\Entity\Tickets;
use App\Entity\Users;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TicketsController extends Controller
{
    /**
     * @Route("/tickets", name="my-tickets")
     */
    public function myTickets()
    {

        $tickets = $this->getDoctrine()
            ->getRepository(Tickets::class)->findBy(
                array(
                    'comprador' => $this->getUser()->getId()
                )
            );

        return $this->render(
            'tickets/my-tickets.html.twig',
            array(
                'titulo'=>'Index',
                'tickets' =>$tickets
            )
        );
    }
}