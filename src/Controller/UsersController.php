<?php
/**
 * Created by PhpStorm.
 * User: belmont
 * Date: 18/2/18
 * Time: 21:17
 */

namespace App\Controller;

use App\Entity\Categorias;
use App\Entity\Provincias;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use App\Entity\Users;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UsersController extends Controller
{
    /**
     * @Route("/users", name="users")
     */
    public function listarUsuarios() {
        $users =  $this->getDoctrine()->getRepository(Users::class)->findAll();
        return $this->render(
            'users/index-users.html.twig',
            array(
                'titulo'=>'Index',
                'usuarios' =>$users
            )
        );
    }


    /**
     * @Route("/users/new", name="users_new")
     */

    //Creación de formulario de nuevo Usuario
    public function formularioUsuario(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

        $nuevoUsuario = new Users();
        $formUser = $this->createFormBuilder($nuevoUsuario,
            array('attr' => array('class' => 'form-signin')))

            ->add('nombre', TextType::class ,
                array('label' => 'Nombre',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Nombre',
                        'trim'=>true)
                )
            )

            ->add('apellidos', TextType::class ,
                array('label' => 'Apellidos',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Apellidos',
                        'trim'=>true)
                )
            )

            ->add('email', EmailType::class ,
                array('label' => 'Email',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Email',
                        'trim'=>true)
                )
            )

            ->add('fecha_nacimiento', DateType::class ,
                array('label' => 'Fecha de Nacimiento', 'attr' =>
                    array('class' => 'form-control mb-3'),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',

                )
            )

            ->add('user', TextType::class ,
                array('label' => 'Username',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Username',
                        'trim'=>true)
                )
            )

            ->add('pass', PasswordType::class ,
                array('label' => 'Password',
                    'attr' => array(
                        'class' => 'form-control mb-3'
                    )
                )
            )


            ->add('avatar', FileType::class ,
                array('label' => 'Avatar del usuario',
                    'attr' => array(
                        'class' => 'form-control mb-3',

                    )
                )
            )

            ->add('Save',SubmitType::class,
                array('label'=>'Crear Usuario', 'attr' =>
                    array('class' => 'btn btn-lg btn-info btn-block')))

            ->getForm();

        $formUser->handleRequest($request);

        if ($formUser->isSubmitted() && $formUser->isValid()) {
            //si es correcto, obtenemos la imagen
            $archivo = $nuevoUsuario->getAvatar();

            //doble número random para que no se repita el nombre de la foto
            $nombreArchivo = rand (0, 1000000). rand (0, 1000000) .'.'.$archivo->guessExtension();

            //events_directory del services.yaml
            $archivo->move($this->getParameter('users_directory'),$nombreArchivo);

            //sobreescribimos la foto
            $nuevoUsuario->setAvatar($nombreArchivo);

            //para encriptar el password
            $password = $passwordEncoder->encodePassword($nuevoUsuario, $nuevoUsuario->getPass());
            $nuevoUsuario->setPass($password);


            $nuevoUsuario->setRole('ROLE_COMPRADOR');



            $em = $this->getDoctrine()->getManager();
            $em->persist($nuevoUsuario);
            $em->flush();
            return $this->redirectToRoute('events');
        }

        return $this->render(
            'users/form-user.html.twig',
            array(
                'titulo'=>'Index',
                'formUser' =>$formUser->createView()
            )
        );

    }

    /**
     * @Route("/users/editar/{id}", name="users_edit")
     */

    //Actualizar de formulario de nuevo Usuario
    public function formularioActualizar(Users $user, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

        $formUser = $this->createFormBuilder($user,
            array('attr' => array('class' => 'form-signin')))

            ->add('nombre', TextType::class ,
                array('label' => 'Nombre',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Nombre',
                        'trim'=>true)
                )
            )

            ->add('apellidos', TextType::class ,
                array('label' => 'Apellidos',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Apellidos',
                        'trim'=>true)
                )
            )

            ->add('email', EmailType::class ,
                array('label' => 'Email',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Email',
                        'trim'=>true)
                )
            )

            ->add('fecha_nacimiento', DateType::class ,
                array('label' => 'Fecha de Nacimiento', 'attr' =>
                    array('class' => 'form-control mb-3'),
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',

                )
            )

            ->add('user', TextType::class ,
                array('label' => 'Username',
                    'attr' => array(
                        'class' => 'form-control mb-3',
                        'placeholder' => 'Username',
                        'trim'=>true)
                )
            )

            ->add('pass', PasswordType::class ,
                array('label' => 'Password',
                    'attr' => array(
                        'class' => 'form-control mb-3'
                    )
                )
            )


            ->add('avatar', FileType::class ,
                array('label' => 'Avatar del usuario',
                    'attr' => array(
                        'class' => 'form-control mb-3',

                    )
                    ,'data_class'=> null
                )
            )


            ->add('Save',SubmitType::class,
                array('label'=>'Crear o modificar Usuario', 'attr' =>
                    array('class' => 'btn btn-lg btn-info btn-block')))

            ->getForm();

        $formUser->handleRequest($request);

        if ($formUser->isSubmitted() && $formUser->isValid()) {
            //si es correcto, obtenemos la imagen
            $archivo = $user->getAvatar();

            //doble número random para que no se repita el nombre de la foto
            $nombreArchivo = rand (0, 1000000). rand (0, 1000000) .'.'.$archivo->guessExtension();

            //events_directory del services.yaml
            $archivo->move($this->getParameter('users_directory'),$nombreArchivo);

            //sobreescribimos la foto
            $user->setAvatar($nombreArchivo);

            //para encriptar el password
            $password = $passwordEncoder->encodePassword($user, $user->getPass());
            $user->setPass($password);


            $user->setRole('ROLE_COMPRADOR');



            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('events');
        }

        return $this->render(
            'users/form-user.html.twig',
            array(
                'titulo'=>'Editar Usuario',
                'user' => $user,
                'formUser' =>$formUser->createView()
            )
        );

    }



    /**
     * @Route("/users/perfil", name="my-perfil")
     */
    public function miPerfil()
    {
        $id = $this->getUser();



        return $this->render(
            'users/user-details.html.twig',
            array(
                'titulo' => 'Mi perfil',
                'users' => $id
            )
        );
    }

    /**
     * @Route("/users/{id}", name="users_id")
     */
    public function userDetail($id)
    {
        $users = $this->getDoctrine()
            ->getRepository(Users::class)->findOneBy(array('id' => $id));
        return $this->render(
            'users/user-details.html.twig',
            array(
                'titulo' => 'Index',
                'users' => $users
            )
        );
    }
}