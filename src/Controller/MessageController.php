<?php
/**
 * Created by PhpStorm.
 * User: belmont
 * Date: 24/2/18
 * Time: 22:02
 */

namespace App\Controller;

use App\Entity\Mensajes;
use App\Entity\Users;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class MessageController extends Controller
{
    /**
     * @Route("message/events", name="mymessage")
     * @Template("message/message.html.twig")
     * @IsGranted("ROLE_COMPRADOR")
     */
    public function muestra()
    {
        $doctrine = $this->getDoctrine();
        $messages = $doctrine->getRepository(Mensajes::class)
            ->findBy(array('destinatario' => $this->getUser()->getId()));

        return ['messages' => $messages];
    }


    /**
     * @Route("message/new", name="newMessage")
     * @Template(":message:form-message.html.twig")
     * @IsGranted("ROLE_COMPRADOR")
     */
    public function nuevo (Request $request)
    {

        $usuarios = $this->getDoctrine()->getRepository(Users::class)->findAll();

        $listaUsuarios = [];
        foreach ($usuarios as $usuario) {
            $listaUsuarios[$usuario->getNombre()] = $usuario->getId();
        }

        $message = new Mensajes();


        $form = $this->createFormBuilder($message)
            ->add('destinatario', ChoiceType::class, [
                'label' => 'Destinatario:',
                'attr' => ['class' => 'form-control'],
                'choices' => $listaUsuarios
            ])
            ->add('mensaje', TextareaType::class, [
                'label' => 'Mensaje:',
                'attr' => ['class' => 'form-control']
            ])
            ->add('Save', SubmitType::class, [
                'label' => 'Enviar',
                'attr' => ['class' => 'btn btn-success btn-block']
            ])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $message->setRemitente($em->find('App:Users', $this->getUser()->getId()));

            $message->setDestinatario($em->find('App:Users', $message->getDestinatario()));

            $em->persist($message);
            $em->flush();

            return $this->redirectToRoute('mymessage');
        }

        return ['form' => $form->createView()];

    }

    /**
     * @Route("message/new/{id}", name="messageTo")
     * @Template("message/form-message.html.twig")
     * @IsGranted("ROLE_COMPRADOR")
     */
    public function newTo(Request $request, Users $user)
    {

        $usuarios = $this->getDoctrine()->getRepository(Users::class)->findAll();

        $listaUsuarios = [];
        foreach ($usuarios as $usuario) {
            $listaUsuarios[$usuario->getNombre()] = $usuario->getId();
        }

        $message = new Mensajes();


        $form = $this->createFormBuilder($message)
            ->add('destinatario', TextType::class, [
                'label' => 'Destinatario:',
                'attr' => ['class' => 'form-control', 'readonly' => 'readonly', 'value' => $user->getNombre()]
            ])
            ->add('mensaje', TextareaType::class, [
                'label' => 'Mensaje:',
                'attr' => ['class' => 'form-control']
            ])
            ->add('Save', SubmitType::class, [
                'label' => 'Enviar',
                'attr' => ['class' => 'btn btn-success btn-block']
            ])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $message->setRemitente($em->find('App:Users', $this->getUser()->getId()));

            $message->setDestinatario($user);

            $em->persist($message);
            $em->flush();

            return $this->redirectToRoute('mymessage');
        }

        return ['form' => $form->createView()];

    }

}
