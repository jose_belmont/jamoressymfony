<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180227181156 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(100) NOT NULL, role VARCHAR(200) NOT NULL, apellidos VARCHAR(200) NOT NULL, fecha_nacimiento DATE NOT NULL, avatar VARCHAR(200) DEFAULT NULL, user VARCHAR(50) NOT NULL, pass VARCHAR(200) NOT NULL, email VARCHAR(200) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE events (id INT AUTO_INCREMENT NOT NULL, categoria INT DEFAULT NULL, provincia INT DEFAULT NULL, creador INT DEFAULT NULL, imagen VARCHAR(600) DEFAULT NULL, titulo VARCHAR(255) NOT NULL, descripcion VARCHAR(500) NOT NULL, fecha DATE NOT NULL, direccion VARCHAR(255) NOT NULL, precio_entrada NUMERIC(10, 2) NOT NULL, entradas_maximas INT NOT NULL, entradas_disponibles INT NOT NULL, INDEX IDX_5387574A4E10122D (categoria), INDEX IDX_5387574AD39AF213 (provincia), INDEX IDX_5387574AA0204913 (creador), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tickets (id INT AUTO_INCREMENT NOT NULL, comprador INT DEFAULT NULL, evento INT DEFAULT NULL, fecha_compra DATE NOT NULL, cantidad INT NOT NULL, precio NUMERIC(10, 2) NOT NULL, coste_total NUMERIC(10, 2) NOT NULL, INDEX IDX_54469DF478571C71 (comprador), INDEX IDX_54469DF447860B05 (evento), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE provincias (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categorias (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574A4E10122D FOREIGN KEY (categoria) REFERENCES categorias (id)');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574AD39AF213 FOREIGN KEY (provincia) REFERENCES provincias (id)');
        $this->addSql('ALTER TABLE events ADD CONSTRAINT FK_5387574AA0204913 FOREIGN KEY (creador) REFERENCES users (id)');
        $this->addSql('ALTER TABLE tickets ADD CONSTRAINT FK_54469DF478571C71 FOREIGN KEY (comprador) REFERENCES users (id)');
        $this->addSql('ALTER TABLE tickets ADD CONSTRAINT FK_54469DF447860B05 FOREIGN KEY (evento) REFERENCES events (id)');
        $this->addSql('ALTER TABLE mensajes ADD remitente INT DEFAULT NULL, ADD destinatario INT DEFAULT NULL, ADD mensaje VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE mensajes ADD CONSTRAINT FK_6C929C8051A5ACA4 FOREIGN KEY (remitente) REFERENCES users (id)');
        $this->addSql('ALTER TABLE mensajes ADD CONSTRAINT FK_6C929C80A7399187 FOREIGN KEY (destinatario) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_6C929C8051A5ACA4 ON mensajes (remitente)');
        $this->addSql('CREATE INDEX IDX_6C929C80A7399187 ON mensajes (destinatario)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574AA0204913');
        $this->addSql('ALTER TABLE mensajes DROP FOREIGN KEY FK_6C929C8051A5ACA4');
        $this->addSql('ALTER TABLE mensajes DROP FOREIGN KEY FK_6C929C80A7399187');
        $this->addSql('ALTER TABLE tickets DROP FOREIGN KEY FK_54469DF478571C71');
        $this->addSql('ALTER TABLE tickets DROP FOREIGN KEY FK_54469DF447860B05');
        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574AD39AF213');
        $this->addSql('ALTER TABLE events DROP FOREIGN KEY FK_5387574A4E10122D');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE events');
        $this->addSql('DROP TABLE tickets');
        $this->addSql('DROP TABLE provincias');
        $this->addSql('DROP TABLE categorias');
        $this->addSql('DROP INDEX IDX_6C929C8051A5ACA4 ON mensajes');
        $this->addSql('DROP INDEX IDX_6C929C80A7399187 ON mensajes');
        $this->addSql('ALTER TABLE mensajes DROP remitente, DROP destinatario, DROP mensaje');
    }
}
